from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in facility_management_website/__init__.py
from facility_management_website import __version__ as version

setup(
	name="facility_management_website",
	version=version,
	description="Website for the Facility Management app",
	author="Ploti.Cloud Devs",
	author_email="dsmwaura@gmail.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
