"""
Configuration for docs
"""

# source_link = "https://github.com/[org_name]/facility_management_website"
# docs_base_url = "https://[org_name].github.io/facility_management_website"
# headline = "App that does everything"
# sub_heading = "Yes, you got that right the first time, everything"

def get_context(context):
	context.brand_html = "Facility Management Website"
