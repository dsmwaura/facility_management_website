from frappe import _

def get_data():
	return [
		{
			"module_name": "Facility Management Website",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Facility Management Website")
		}
	]
