
const propertiesElement = document.getElementById('charactersList');
const searchBar = document.getElementById('searchBar');
const navSection = document.getElementById("mySidenav")
const navDisplayArea = document.getElementById("sideNavContent")
const newStartDiv = document.getElementById("newstart")




console.log(searchBar)
let propertyList = [];

function closeNav() {
    navDisplayArea.innerHTML = ''
    navSection.style.width = "0%";
    newStartDiv.style.hidden = false

}

let formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'KES',
});

const getPropertyHTML = (property) => {
    let imgSrc = property.house_image || "https://api.rentonkenya.com/storage/images/2021/12/15/avmEbQ29dYe3oueUQ7oLcf033pOm6SXwyZFpEgr9.jpg"
    let imgSection = `<img src="${imgSrc}" width="40%" height="40%" />`
    let detailSection = `<hr><h3>${property.title}</h3><h4>${formatter.format(parseFloat(property.rental_rate))}</h4>`
    return `<div>${imgSection} ${detailSection}</div>`
}
const getFormComponent = (property) => {
    // console.log(property,"Is selected")
    let context = property.name
    console.log(context)
    return `
    <form class="border border-light p-5" id="rentalApplicationForm"  action="javascript:void(0)";>

    <p class="h4 mb-4 text-center">Make Application</p>

    <input type="text" id="fullName" class="form-control mb-4" placeholder="Full Name">
    <input type="email" id="emailAddress" class="form-control mb-4" placeholder="E-mail">
    <input type="text" id="mobilePhone" class="form-control mb-4" placeholder="Full Name">

    <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3" placeholder="Message"></textarea>


    <input placeholder="Selected date" type="text" id="datepicker" class="form-control datepicker">


    <div class="d-flex justify-content-between">
       
      
    </div>

    <button onclick="postFormData('${context}')" class="btn btn-info btn-block my-4" type="submit">Send</button>

    
</form>

    `
}
const postFormData =(propertyID)=>{
    console.log(propertyID)
    const form = document.getElementById("rentalApplicationForm")
    const formElement = document.querySelector('rentalApplicationForm');
    //  const formData = new FormData(formElement);

    console.log(formData)
}

function openNav(apt) {
    navDisplayArea.innerHTML = ""
    selectedProperty = propertyList.message.filter(prop => prop.name === apt)[0]
    console.log(selectedProperty)
    navDisplayArea.innerHTML = `<div class="row" style="text_align:center">
                                    <h4>${formatter.format(parseFloat(selectedProperty.rental_rate))}</h4>
                                </div>
                                <div class="row">
                                    <div class="col-8">${getPropertyHTML(selectedProperty)}</div>
                                    <div class="col-4">${getFormComponent(selectedProperty)}</div>
                                </div>
                                `
    navSection.style.width = "95%";
    newStartDiv.style.hidden = true

}


searchBar.addEventListener('keyup', (e) => {
    const searchString = e.target.value.toLowerCase();

    const filteredProperties = propertyList.message.filter((character) => {
        return (
            character.property_group.toLowerCase().includes(searchString) ||
            character.title.toLowerCase().includes(searchString)
        );
    });
    displayProperties(filteredProperties);
});

const loadProperties = async () => {
    try {
        const res = await fetch('/api/method/facility_management_website.api.tenant_renting.get_vacant_properties');
        propertyList = await res.json();
        displayProperties(propertyList.message);
    } catch (err) {
        console.error(err);
    }
};
const defaultString = "<h4>No vacant properties for now</h4>"
const displayProperties = (propertyList) => {
    const htmlString = propertyList
        .map((character) => {
            let context = character.name
            return `
            <li class="character">
            <div class="card">
            <div class="card-img"
                style="background-image:url(https://pictures-kenya.jijistatic.com/9022525_20200521-165710_1500x729.jpg);">
                <div class="overlay">
                    <div class="overlay-content">
                        <a onclick="openNav('${context}')" class="hover">View Rental</a>
                    </div>
                </div>
            </div>
            <div class="card-content">

                <a href="#!">
                    <h2>${character.title} in ${character.property_group}</h2>
                    <!-- <p>Lorem ipsum dolor sit amet consectetur, lorem ipsum dolor</p> -->

                </a>
                <h5><span class="badge badge-danger">For Rent</span></h5>
                <div class="property-price">
                    <span>${character.rental_rate}/${character.rental_frequency}</span>
                </div>
                <div>
                    <ul id="room_space">
                    <li>${character.bathroom} Bedrooms</li>
                    <li> | </li>
                    <li>${character.bedroom} Bath</li>
                    </ul>
                </div>
            </div>
        </div>
            </li>
        `;
        })
        .join('') || defaultString;
    propertiesElement.innerHTML = htmlString;
};

loadProperties();
// console.log(propertyList)
